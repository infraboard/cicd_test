FROM registry.cn-hangzhou.aliyuncs.com/godev/golang:1.22 AS builder

LABEL stage=gobuilder

WORKDIR /src
COPY go.mod .
COPY go.sum .

ENV CGO_ENABLED=0 \
GOOS=linux \
GOARCH=amd64 \
GOPROXY=https://goproxy.cn,direct
#GOPRIVATE="*.gitlab.com"

# 下载依赖
RUN go mod download
# 执行构建
COPY . .
RUN go build -o test_api main.go

FROM registry.cn-hangzhou.aliyuncs.com/godev/alpine:latest
WORKDIR /app
EXPOSE 8080
COPY --from=builder /src/test_api /app/test_api

CMD ["./test_api"]